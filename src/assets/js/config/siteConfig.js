"use strict"

var configuration = {
  name : "GEF Master Template",
  components : {

  },
  requireConfig : {

  }
}

// Update RequireJS config options
require.config(configuration.requireConfig)

export default configuration